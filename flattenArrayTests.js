QUnit.test("Happy Path: Array With No Nesting Should Return The Same Array", function( assert ) {
	var flattenedArray = flattenArray([1, 2, 3, 4, 5, 6, 12, 23, 99, 55, 18, 54, 7, 15, 16]); 
	var expectedArray = [1, 2, 3, 4, 5, 6, 12, 23, 99, 55, 18, 54, 7, 15, 16];

  	assert.deepEqual( flattenedArray, expectedArray, "Passed: The array was flattened" );
});

QUnit.test("Happy Path: Array That Is Nested Four Levels Deep Should Flatten All Nested Arrays", function( assert ) {
	var flattenedArray = flattenArray([1, 2, 3, 4, [5, 6, [12, 23, [99, 55, 18], 54], 7], 15, 16]); 
	var expectedArray = [1, 2, 3, 4, 5, 6, 12, 23, 99, 55, 18, 54, 7, 15, 16];

  	assert.deepEqual( flattenedArray, expectedArray, "Passed: The array was flattened" );
});

QUnit.test("Happy Path: Array That Has Several Nested Multilevel Nested Arrays Should Flatten All Nested Arrays", function( assert ) {
	var flattenedArray = flattenArray([1, 2, 3, 4, [5, 6, [12, 23, [99, 55, 18], 54], 7], 15, [66, 78, [33]], 16, 35, [31, 24]]); 
	var expectedArray = [1, 2, 3, 4, 5, 6, 12, 23, 99, 55, 18, 54, 7, 15, 66, 78, 33, 16, 35, 31, 24];

  	assert.deepEqual( flattenedArray, expectedArray, "Passed: The array was flattened" );
});

QUnit.test("Sad Path: Null Element Should Throw An Exception", function( assert ) {
	var invalidValue = null;
	var executeFunction = function(){ flattenArray([1, 2, 3, 4, [5, 6, [12, 23, [99, invalidValue, 18], 54], 7], 15, 16]) }
	var exceptionHandler = function(error){  return error.name === "FlattenArrayException" && error.value === invalidValue }; 

	assert.throws(
		executeFunction, 
		exceptionHandler,
		"Passed: The expected exception was thrown" );
});

QUnit.test("Sad Path: String Element Should Throw An Exception", function( assert ) {
	var invalidValue = "Bad Value";
	var executeFunction = function(){ flattenArray([1, 2, 3, 4, [5, 6, [12, 23, [invalidValue, 45, 18], 54], 7], 15, 16]) }
	var exceptionHandler = function(error){ return error.name === "FlattenArrayException" && error.value === invalidValue; }
	
	assert.throws(
		executeFunction, 
		exceptionHandler,
		"Passed: The expected exception was thrown" );
});

QUnit.test("Sad Path: Empty Array Should Throw An Exception", function( assert ) {
	var invalidValue = [];
	var executeFunction = function(){ flattenArray([1, 2, 3, invalidValue, [5, 6, [12, 23, 54], 7], 15, 16]) }
	var exceptionHandler = function(error){  return error.name === "FlattenArrayException"  && error.value === invalidValue; }
	
	assert.throws(
		executeFunction, 
		exceptionHandler,
		"Passed: The expected exception was thrown" );
});

// The requirement used the word 'integers' so I took that strictly to mean no casting number strings to numbers.
QUnit.test("Sad Path: String Number Should Throw An Exception", function( assert ) {
	var invalidValue = "45";
	var executeFunction = function(){ flattenArray([1, 2, 3, [5, 6, [12, 23, 54], 7], 15, invalidValue, 16]) }
	var exceptionHandler = function(error){  return error.name === "FlattenArrayException"  && error.value === invalidValue; }
	
	assert.throws(
		executeFunction, 
		exceptionHandler,
		"Passed: The expected exception was thrown" );
});