function FlattenArrayException(message, value, location){
	this.name = "FlattenArrayException"
	this.value = value;
	this.message = message;
}

// Right now this function is hostile to empty or null arrays, however if we wanted to we could make it ignore empty or null sub arrays rather than erroring out.
// So a value like [1, 2, 3,[4,[],5,6]] would get flattened to 1,2,3,4,5,6. This could be done by adding a level parameter to the function that initializes to 0 but is passed as level + 1 during recursion.
function flattenArray(array){
	if(!Array.isArray(array)){ throw new FlattenArrayException("Value " + array + " is not an array", array) }
	if(array.length === 0) { throw new FlattenArrayException("Value is empty", array)}

	var finalFlattenedArray = [];
	
	array.forEach(function(currentElement){ // Depending on browser/performance requirements an old-fashioned for-loop may be a better choice here
		if(Array.isArray(currentElement)){
      		var flattenedSubArray = flattenArray(currentElement)
			finalFlattenedArray = finalFlattenedArray.concat(flattenedSubArray);
		}// Since null is converted to 0 isNaN will not trip on it. Also checking for type string to strictly enforce integer requirement. Otherwise, isNaN will allow "43" through.
		else if(typeof currentElement === "string" || isNaN(currentElement) || currentElement === null){ 
			throw new FlattenArrayException( "Value " + currentElement + " is not a number!", currentElement);
		}
		else{
			finalFlattenedArray.push(currentElement);
		}
	});

	return finalFlattenedArray;
}
